const evtSource = new EventSource("/subscribe");

evtSource.onmessage = (message) => {
  console.log("Message");

  if (message.data === "refresh") {
    location.reload();
  }
};

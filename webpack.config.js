const devConfig = require("./config/webpack.dev");
const prodConfig = require("./config/webpack.prod");

module.exports = function (env, argv) {
  if (argv.mode === "production") {
    return prodConfig;
  }

  return devConfig;
};

const TerserPlugin = require("terser-webpack-plugin");
const { merge } = require("webpack-merge");
const common = require("./webpack.common");

module.exports = merge(
  {
    optimization: {
      minimize: true,
      minimizer: [new TerserPlugin()],
    },
  },
  common
);

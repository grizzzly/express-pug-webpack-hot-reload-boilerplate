const path = require("path");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

const resolvePath = (source) => path.resolve(__dirname, source);

module.exports = {
  entry: {
    index: [
      resolvePath("../assets/ts/pages/index.ts"),
      resolvePath("../assets/scss/pages/index.scss"),
    ],
    404: resolvePath("../assets/scss/pages/404.scss"),
    common: resolvePath("../assets/scss/common.scss"),
    dev_reload: resolvePath("../assets/ts/dev_reload.ts"),
  },
  module: {
    rules: [
      {
        test: /\.(ts|js)x?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader",
          },
        ],
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "css/[name].css",
            },
          },
          {
            loader: "extract-loader",
          },
          {
            loader: "css-loader",
          },
          {
            loader: "postcss-loader",
          },
          {
            loader: "sass-loader",
          },
        ],
      },
    ],
  },
  output: {
    filename: "js/[name].js",
    path: resolvePath("../public"),
  },
  resolve: {
    extensions: [".ts", ".js", "css", "scss"],
  },
  plugins: [new CleanWebpackPlugin()],
};

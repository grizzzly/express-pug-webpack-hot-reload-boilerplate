import { Router, Response } from "express";

const chokidarRouter = Router();

const clients: Response[] = [];

chokidarRouter.get("/subscribe", (req, res) => {
  res.set({
    "Cache-Control": "no-cache",
    "Content-Type": "text/event-stream",
    Connection: "keep-alive",
  });
  res.flushHeaders();

  clients.push(res);

  req.on("close", () => {
    const index = clients.findIndex(($res) => $res == res);
    if (index >= 0) {
      clients.splice(index, 1);
    }
  });
});

export { chokidarRouter, clients };

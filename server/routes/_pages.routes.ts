import { Router } from "express";
import * as fs from "fs";
import * as path from "path";

const IS_DEV = process.env.NODE_ENV !== "production";

const products = [
  { title: "Some title" },
  { title: "Some title" },
  { title: "Some title" },
];

const pagesRouter = Router();

pagesRouter.get("/", (req, res) => {
  res.render("pages/index", {
    products,
    pagetitle: "Home",
    dev: IS_DEV,
  });
});

pagesRouter.get("*", (req, res) => {
  const filePath = path.join(
    __dirname,
    "../",
    "../",
    "views",
    "pages",
    req.path + ".pug"
  );

  try {
    if (fs.existsSync(filePath)) {
      res.render(`pages${req.path}`);
    } else {
      res.render(`pages/404`);
    }
  } catch (err) {
    res.render(`pages/404`);
  }
});

export { pagesRouter };

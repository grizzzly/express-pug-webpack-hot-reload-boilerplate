import * as express from "express";
import * as cors from "cors";
import { chokidarRouter, clients, pagesRouter } from "./routes";
import * as chokidar from "chokidar";
import * as path from "path";

const VIEWS_PATH = path.join(__dirname, "..", "views");
const PUBLIC_PATH = path.join(__dirname, "..", "public");

const PORT = process.env.PORT || 3001;

export class App {
  private server = express();

  constructor() {
    this.server.use(cors());
    this.server.use(express.json());
    this.server.use(express.urlencoded({ extended: true }));
    this.server.use(express.static("public"));

    this.server.locals.env = process.env;

    this.server.set("view engine", "pug");

    this.setupChokidar();
    this.server.use(pagesRouter);
  }

  setupChokidar() {
    this.server.use(chokidarRouter);

    function sendRefresh() {
      clients.forEach((client) => {
        client.write("data: refresh");
        client.write("\n\n");
      });
    }

    chokidar.watch([VIEWS_PATH, PUBLIC_PATH]).on("all", () => {
      sendRefresh();
    });
  }

  start() {
    this.server.listen(PORT, () => {
      console.log(`App is running on http://localhost:${PORT}`);
    });
  }
}
